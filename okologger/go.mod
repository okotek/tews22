module okotech.net/x/okologger

go 1.18

require (
	okotech.net/x/okoconf v1.1.1
	okotech.net/x/okolog v1.1.1
)

replace okotech.net/x/okoconf => ../okoconf

replace okotech.net/x/okolog => ../okolog
