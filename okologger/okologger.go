package main

import (
	"fmt"
	"time"

	"okotech.net/x/okoconf"
	"okotech.net/x/okolog"
)

var CFG okoconf.CfgCont

func main() {

	go okolog.PrintLogger()

	go okolog.PerfRec()

	CFG.SetDefault()
	eChan := make(chan okolog.ErrorStruct)
	lChan := make(chan okolog.LogStruct)

	go func() {
		for i := range eChan {
			fmt.Println(i)
		}
	}()
	go func() {
		for i := range lChan {
			fmt.Println(i)
		}
	}()

	fmt.Println("Starting listeners:")
	go okolog.ListenForErrors(eChan, CFG)
	go okolog.ListenForLogs(lChan, CFG)
	for {
		time.Sleep(1999 * time.Hour)
	}
}
