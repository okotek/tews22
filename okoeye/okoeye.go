package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gocv.io/x/gocv"
	"okotech.net/x/okoconf"
	"okotech.net/x/okoframe"
	"okotech.net/x/okolog"
	"okotech.net/x/okonet"
)

var USERNAME string
var PASSHASH []byte
var CFG okoconf.CfgCont

func main() {

	go okolog.PrintLogger()

	stageOneChan := make(chan gocv.Mat)
	stageTwoChan := make(chan okoframe.Frame)
	CFG.ExeName = os.Args[0]

	tarcam := flag.Int("t", 0, "V4L device to use as the input camera.")
	flag.Parse()

	go runCam(stageOneChan, *tarcam)
	go preProcessor(stageOneChan, stageTwoChan)
	go okonet.Sendoff(stageTwoChan, "localhost:8081", CFG)

	for {
		time.Sleep(10 * time.Hour)
	}
}

func runCam(stageOneChan chan gocv.Mat, targetCamera int) {

	fmt.Println("Starting runcam")

	tmpImg := gocv.NewMat()
	cam, err := gocv.VideoCaptureDevice(targetCamera)

	if err != nil {
		fmt.Println("Error getting video device: ", err)
		time.Sleep(2 * time.Second)
		runCam(stageOneChan, targetCamera)
		return
	}

	for {
		cam.Read(&tmpImg)
		stageOneChan <- tmpImg
		time.Sleep(3 * time.Second)
	}

}

func preProcessor(inputChan chan gocv.Mat, outputChan chan okoframe.Frame) {

	for tmpMat := range inputChan {
		sendFrame := okoframe.FrameFromMat(tmpMat)
		sendFrame.UserName = USERNAME
		sendFrame.UserHash = PASSHASH
		sendFrame.ChanCount = tmpMat.Channels()

		//sendFrame.MotionMap = okoframe.GetMotionMap(tmpMat)

		outputChan <- sendFrame
	}

}
