package main

import (
	"fmt"
	"time"

	"okotech.net/x/okoconf"
	"okotech.net/x/okoframe"
	"okotech.net/x/okonet"
)

func main() {
	cfg := okoconf.SetConfig()

	thePipe := make(chan okoframe.Frame)
	thePipeBoog := make(chan okoframe.Frame)

	go okonet.Listen(thePipeBoog, ":8081", cfg)
	time.Sleep(2 * time.Second)
	go okonet.Sendoff(thePipe, "localhost:8081", cfg)

	go func() {
		for {
			myFrame := okoframe.GenBlank()
			thePipe <- myFrame
		}
	}()
	for tmp := range thePipeBoog {
		fmt.Println(tmp.Name)
	}
}
