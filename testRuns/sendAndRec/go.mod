module okotech.net/x/sar

go 1.18

require okotech.net/x/okoconf v1.1.1

require okotech.net/x/okoframe v1.1.1

require okotech.net/x/okonet v1.1.1

require gocv.io/x/gocv v0.31.0 // indirect

replace okotech.net/x/okoconf => ../../okoconf

replace okotech.net/x/okoframe => ../../okoframe

replace okotech.net/x/okonet => ../../okonet
