package main

import (
	"fmt"

	"gocv.io/x/gocv"

	"okotech.net/x/okoframe"
)

func main() {

	tmpMat := gocv.IMRead("../testImg.jpg", 1)
	tmpFrm := okoframe.FrameFromMat(tmpMat)

	fmt.Println(okoframe.GetMotionMap(tmpFrm, tmpFrm, 15, 0))

}
