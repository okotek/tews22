module okotech.net/x/bt

go 1.18

require gocv.io/x/gocv v0.31.0 // indirect

require okotech.net/x/okoframe v1.1.1

replace okotech.net/x/okoframe => ../../okoframe
