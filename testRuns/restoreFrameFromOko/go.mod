module okotech.net/x/rffo

go 1.18

require okotech.net/x/okoframe v1.1.1

require gocv.io/x/gocv v0.31.0 // indirect

replace okotech.net/x/okoframe => ../../okoframe
