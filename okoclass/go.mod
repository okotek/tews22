module gitlab.com/ashinnv/okoclass

go 1.13

require (
	gocv.io/x/gocv v0.31.0
	okotech.net/x/okoconf v1.1.1
	okotech.net/x/okoframe v1.1.1
	okotech.net/x/okolog v1.1.1
	okotech.net/x/okonet v1.1.1
)

replace okotech.net/x/okoframe => ../okoframe

replace okotech.net/x/okonet => ../okonet

replace okotech.net/x/okoconf => ../okoconf

replace okotech.net/x/okolog => ../okolog

replace gitlab.com/ashinnv/oddstring => ../oddstring/
