package main

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gocv.io/x/gocv"
	"okotech.net/x/okoconf"
	"okotech.net/x/okoframe"
	"okotech.net/x/okolog"
	"okotech.net/x/okonet"
)

var CFG okoconf.CfgCont
var sendErr func(string)
var sendLog func(string)

func main() {

	go okolog.PrintLogger()

	sendErr, sendLog = okolog.InitLogServices(CFG)

	stageOne := make(chan okoframe.Frame)
	stageTwo := make(chan okoframe.Frame)

	go okonet.Listen(stageOne, ":8082", CFG)
	go detect(stageOne, stageTwo, "./classifier.xml")
	go okonet.Sendoff(stageTwo, "localhost:8083", CFG)

	stopRec()
}

/*
** BROKEN: GETTING CONCURRENT WRITE ERRORS FOLLOWED BY A CRASH.
** NEED TO MUX THE ACCESS TO DETMAP TO PREVENT THIS.
**
 */
func detect(input chan okoframe.Frame, output chan okoframe.Frame, classifierLocation string) {

	for frm := range input {
		fmt.Println("FRM IN")

		go func() {

			sendLog("Starting detect loop.")
			//detMap := make(map[string][]image.Rectangle)
			//tmpRect := []image.Rectangle{image.Rectangle{image.Point{0, 0}, image.Point{1, 1}}}
			//detMap["init"] = tmpRect
			var tmpImg gocv.Mat
			var sublError error
			//(gocv.Mat, string, []byte, map[int]image.Rectangle, map[string][]image.Rectangle, error)
			tmpImg, _, _, _, detMap, _, sublError := frm.Sublimate()

			fname := filepath.Base(classifierLocation)

			fmt.Println("Loading classifier from :", fname)

			det := gocv.NewCascadeClassifier()
			if !det.Load(fname) {
				fmt.Errorf("Couldn't load classifier.")
				os.Exit(1)
			}

			rects := det.DetectMultiScale(tmpImg)
			detMap[fname] = rects

			frm.Detections = detMap

			if sublError != nil {
				fmt.Println("Big error with sublimate stuff:", sublError)
				sendErr("Big error with sublimate stuff: " + sublError.Error())
				frm.ErrList = append(frm.ErrList, sublError)
			}

			output <- frm
			return
		}()
	}
}

func stopRec() {
	for {
		time.Sleep(1000 * time.Hour)
	}
}
