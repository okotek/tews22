package main

import (
	"fmt"
	"image"
	"image/color"
	"time"

	"gocv.io/x/gocv"

	"okotech.net/x/okoconf"
	"okotech.net/x/okoframe"
	"okotech.net/x/okonet"
)

var CFG okoconf.CfgCont

func main() {
	pipeStage1 := make(chan okoframe.Frame)
	pipeStage2 := make(chan okoframe.Frame)

	go okonet.Listen(pipeStage1, ":8083", CFG)

	for i := 0; i < stampers; i++ {
		go stamp(pipeStage1, pipeStage2)
	}

	go okonet.Sendoff(pipeStage2, ":8084", CFG)

	for {
		time.Sleep(time.Hour * 100)
	}
}

func stamp(input chan okoframe.Frame, output chan okoframe.Frame) {
	blue := color.RGBA{0, 0, 255, 0}
	for tmp := range input {

		tmpMt, err := gocv.NewMatFromBytes(tmp.Wid, tmp.Hei, tmp.Type, tmp.MatBytes)
		if err != nil {
			fmt.Println("Error generating frame in stamp machine:", err)
		}

		for key, rects := range tmp.Detections {

			for _, r := range rects {
				gocv.Rectangle(&tmpMt, r, blue, 3)

				size := gocv.GetTextSize("txt", gocv.FontHersheyPlain, 1.2, 2)
				pt := image.Pt(r.Min.X+(r.Min.X/2)-(size.X/2), r.Min.Y-2)
				gocv.PutText(&tmpMt, key, pt, gocv.FontHersheyPlain, 1.2, blue, 2)
			}
		}
		tmp <- output
	}
}
