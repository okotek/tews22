module okotech.net/x/okostamp

go 1.13

replace okotech.net/x/okonet => ../okonet

replace okotech.net/x/okoframe => ../okoframe

require (
	gocv.io/x/gocv v0.31.0
	okotech.net/x/okoconf v0.0.0-00010101000000-000000000000
	okotech.net/x/okoframe v0.0.0-00010101000000-000000000000
	okotech.net/x/okonet v0.0.0-00010101000000-000000000000
)

replace okotech.net/x/okoconf => ../okoconf
