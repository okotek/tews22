package okoMofinFuncs

import (
	"fmt"

	"asdf.com/x/okoframe"
)

type Pxl4 struct {
	R byte
	G byte
	B byte
	A byte
}

type Pxl3 struct {
	R byte
	G byte
	B byte
}

type Pxl1 struct {
	V byte
}

//NEED TO UPDATE COMPARISON PIXEL MATRIX
//Need to put generics to use here
//MofinInd is a motion finder for a specific user/camera combo. Takes input chan for its user/cam combo and shares output chan between all funcs
func MofinInd(input chan okoframe.Frame, output chan okoframe.Frame, compType string) {
	initFrame := <-input

	fmt.Println("Beware, comp type not fully implemented:", compType)
	chanCount := initFrame.ChanCount
	pixlCount := len(initFrame.MatBytes) / chanCount

	if chanCount == 3 {

		pixelMats := [2][]Pxl3{} //Dim 1 is the individual images. Dimension 2 is the slice of pixels

		tmpPxl := Pxl3{}

		//Initialize the comp frame pixel map
		for i := 0; i < len(initFrame.MatBytes); i = i + 3 {
			tmpPxl.R = initFrame.MatBytes[i+0]
			tmpPxl.G = initFrame.MatBytes[i+1]
			tmpPxl.B = initFrame.MatBytes[i+2]

			pixelMats[0] = append(pixelMats[0], tmpPxl)
		}

		for currentFrame := range input {

			tmpPxl = Pxl3{}
			subPxlDiff := 0
			pixlDiff := 0

			for i := 0; i < len(currentFrame.MatBytes); i = i + 3 { //Build a list of pixels
				tmpPxl.R = currentFrame.MatBytes[i+0]
				tmpPxl.G = currentFrame.MatBytes[i+1]
				tmpPxl.B = currentFrame.MatBytes[i+2]

				pixelMats[1] = append(pixelMats[1], tmpPxl)
			}

			//Compare the current frame's pixel matrix and the comparison one.
			for pxlNdx, currPxl := range pixelMats[1] {
				rDiff := pixelMats[0][pxlNdx].R - currPxl.R
				gDiff := pixelMats[0][pxlNdx].G - currPxl.G
				bDiff := pixelMats[0][pxlNdx].B - currPxl.B

				if rDiff != 0 {
					subPxlDiff++
				}
				if gDiff != 0 {
					subPxlDiff++
				}
				if bDiff != 0 {
					subPxlDiff++
				}

				if compType == "all" {
					if subPxlDiff > 2 {
						pixlDiff++
					}
				} else if compType == "one" {
					if subPxlDiff > 0 {
						pixlDiff++
					}
				} else {
					fmt.Println("Wrong pixel comp type:", compType)
					return
				}
			}

			//motion percent applied to frame
			motPer := pixlDiff / pixlCount
			currentFrame.MotPer = motPer

			output <- currentFrame

		}
	} else {
		fmt.Println("Wrong type of pixel. Not implimented yet.")
	}

}

//ManageMotionFinders matches independent user/camera combos and assigns their frames to motion finding funcs
func ManageMotionFinders(inFrames chan okoframe.Frame, outChan chan okoframe.Frame) {

	//Map of chans to functions that have been started
	chanMap := make(map[string](chan okoframe.Frame))

	for cFrame := range inFrames {
		if _, ok := chanMap[okoframe.GetUUC(cFrame)]; !ok {
			//If there isn't already an entry for this user/unit/camera combo
			newChan := make(chan okoframe.Frame)

			go MofinInd(newChan, outChan)
			chanMap[okoframe.GetUUC(cFrame)] = newChan

			newChan <- cFrame
		} else {
			//Find chan to this user's goroutine
			chanMap[okoframe.GetUUC(cFrame)] <- cFrame
		}
	}

}
