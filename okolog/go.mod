module okotech.net/x/okolog

go 1.18

require (
    okotech.net/x/okoconf v1.1.1
    )

replace okotech.net/x/okoconf => ../okoconf
