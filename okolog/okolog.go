package okolog

import (
	"encoding/gob"
	"fmt"
	"net"
	"os"
	"runtime"
	"time"

	"okotech.net/x/okoconf"
)

///PerfStruct is for logging performance figures
type PerfStruct struct {
	FunctionMemoryUseMap map[string]uint64 //list of functions and the memory they use
	ProcessorTime        float64           //Percent processor use for process
	TotalMemory          uint64
	FileName             string
	ExeName              string
	CallTime             int64
	LineNumber           int
}

///ErrorStruct is for logging errors
type ErrorStruct struct {
	ErrorMessage string //Meat and potatoes
	ErrorLevel   int    //Not implemented
	FileName     string
	ExeName      string
	CallTime     int64 //Timestamp this was called
	LineNumber   int
}

///LogStruct is for non-error logs
type LogStruct struct {
	Message    string
	FileName   string
	ExeName    string
	CallTime   int64
	LineNumber int
}

///CounterStruct is for logging the number of times that something has happened
type CounterStruct struct {
	Name    string //Name of thing being counted
	Line    int    //Line in file
	ExeName string //Name of thing being run
}

/*
** errorFunc,LogFunc := InitLogServices(config)
**
** This function is meant to be called at the beginning of the program, at
** the top of the file. It returns two functions that can be called to send
** off logs and errors after setting up the sendoff functions for them. This func
** is not necessary, as ErrorSender() and GeneralLoggerSender() could be started
** independently, but provides an easier means of getting it all set up.
 */
func InitLogServices(c okoconf.CfgCont) (func(input string), func(input string)) {

	c.LoggerAddr = "localhost:9091"
	c.LoggerListenPort = ":9091"
	c.ErrorLoggerAddr = "localhost:9092"
	c.ErrListenPort = ":9092"
	c.ExeName = "DEPRECIATED"

	eChan := make(chan ErrorStruct) //error
	lChan := make(chan LogStruct)   //log

	go ErrorSender(eChan, c)
	go GeneralLoggerSender(lChan, c)

	return GenerateErrorCollector(eChan), GenerateGeneralLogCollector(lChan)

}

/*
** ErrorSender() gets started early in the program before any collectors are called. It
** begins listening on a channel for error structs to come to it. From there, it fires off
** a new goroutine to encode and transmit that error struct to the listening server.
 */
func ErrorSender(sendout chan ErrorStruct, cnf okoconf.CfgCont) {
	sndConn, err := net.Dial("tcp", cnf.ErrorLoggerAddr)

	if err != nil {
		fmt.Println("ERROR DIALING ERROR RECEIVER:[", err, "] Data dumped. Retrying.")
		//close(sendout)
		ErrorSender(sendout, cnf)
		time.Sleep(2 * time.Second)
		return
	}
	for toSend := range sendout {

		//go func() {
		//fmt.Println("Dialing Error Addr:", cnf.ErrorLoggerAddr)
		enc := gob.NewEncoder(sndConn)
		encerr := enc.Encode(toSend)
		if encerr != nil {
			sndConn.Close()
			fmt.Println("ERROR ENCODING FOR ERROR RECEIVER:", encerr, "Data dumped. Retrying.")
			//close(sendout)
			ErrorSender(sendout, cnf)
			time.Sleep(2 * time.Second)
			return
		}
		//}()
	}
}

/*
	GenerateErrorCollector returns a function that can be called that
	works by taking an argument that represents the error that we want
	to report and sends it down a chan that GEC() provided to the ErrorSender
	function for sendoff. This is meant to be used after the ErrorSender() function
	is started.
*/
func GenerateErrorCollector(sendChan chan ErrorStruct) func(input string) {
	return func(input string) {

		_, fName, line, _ := runtime.Caller(0)

		retDat := ErrorStruct{
			input,
			0,
			fName,
			os.Args[0],
			time.Now().UnixNano(),
			line,
		}

		sendChan <- retDat
		return

	}
}

func GeneralLoggerSender(sendout chan LogStruct, cnf okoconf.CfgCont) {

	sndConn, err := net.Dial("tcp", cnf.LoggerAddr)
	defer sndConn.Close()

	fmt.Println("Dialing Logger:", cnf.LoggerAddr)
	if err != nil {
		fmt.Println("ERROR DIALING LOGGER RECEIVER:", err, "Data dumped. Retrying.")
		//close(sendout)
		GeneralLoggerSender(sendout, cnf)
		time.Sleep(2 * time.Second)
		return
	}

	for toSend := range sendout {

		//go func() {
		enc := gob.NewEncoder(sndConn)
		encErr := enc.Encode(toSend)
		if encErr != nil {
			sndConn.Close()
			fmt.Println("ERROR ENCODING FOR LOGGER RECEIVER:", encErr, "Data dumped. Retrying.")
			//close(sendout)
			GeneralLoggerSender(sendout, cnf)
			time.Sleep(2 * time.Second)
			return
		}
		//}()
	}
}

func GenerateGeneralLogCollector(sendChan chan LogStruct) func(input string) {
	return func(input string) {

		_, fName, line, _ := runtime.Caller(0)

		retDat := LogStruct{
			input,
			fName,
			os.Args[0],
			time.Now().UnixNano(),
			line,
		}

		sendChan <- retDat
		return

	}
}

/*
** ListenForErrors() accepts a channel to send the ErrorStructs out on.
** It then listens on cfg.ErrListenPort for incoming.
 */
func ListenForErrors(output chan ErrorStruct, cfg okoconf.CfgCont) {

	fmt.Println("Listening for errors on:", cfg.ErrListenPort)

	ln, err := net.Listen("tcp", cfg.ErrListenPort)
	if err != nil {
		fmt.Println("Error starting listen:", err, "Retrying")
		ListenForErrors(output, cfg)
		return
	}

	for {
		con, err := ln.Accept()
		if err != nil {
			fmt.Println("Accept failed:", err)
		}

		/*go*/
		func() {

			var tmpErr ErrorStruct

			dec := gob.NewDecoder(con)
			decErr := dec.Decode(&tmpErr)
			if decErr != nil {
				con.Close()
				fmt.Println("Error decoding error:", decErr)
				return
			}

			output <- tmpErr

			con.Close()

		}()

	}
}

/*
** ListenForLogs() accepts a channel to send the LogStructs out on.
** It then listens on cfg.LoggerListenPort for incoming.
 */
func ListenForLogs(output chan LogStruct, cfg okoconf.CfgCont) {

	ln, err := net.Listen("tcp", cfg.LoggerListenPort)
	if err != nil {
		fmt.Println("Error starting listen:", err, "Retrying")
		ListenForLogs(output, cfg)
		return
	}

	for {
		con, err := ln.Accept()
		if err != nil {
			fmt.Println("Accept failed:", err)
		}

		go func(con net.Conn) {
			defer con.Close()

			var tmpLog LogStruct

			dec := gob.NewDecoder(con)
			decErr := dec.Decode(&tmpLog)
			if decErr != nil {
				fmt.Println("Error decoding error:", decErr)
				return
			}

			output <- tmpLog
			con.Close()
			return

		}(con)

	}
}

func bToMb(b int) int {
	return b / 1024 / 1024
}

func PerfRec() {
	lsnPrt := ":8093"
	var tmpMp map[string]uint64

	var rec PerfStruct = PerfStruct{
		tmpMp,
		0,
		0,
		"",
		"",
		0,
		0,
	}

	recCon, recErr := net.Listen("tcp", lsnPrt)
	defer recCon.Close()

	if recErr != nil {
		fmt.Println("Error listening:", recErr)
	} else {
		fmt.Println("Performance listening on port:", lsnPrt)
	}

	for {
		conn, conErr := recCon.Accept()
		if conErr != nil {
			fmt.Println("Error setting up for connection: ", conErr)
		}
		dec := gob.NewDecoder(conn)
		decErr := dec.Decode(&rec)
		if decErr != nil {
			fmt.Println("Error executing decode:", decErr)
		}

		fmt.Println("PerfStuff:", rec)
	}
}

func PerfSender() {
	targAddr := "localhost:8093"
	var m runtime.MemStats
	var tmpMp map[string]uint64

	var p PerfStruct = PerfStruct{
		tmpMp,
		0,
		0,
		"",
		"",
		0,
		0,
	}

	for {
		runtime.ReadMemStats(&m)
		p.FunctionMemoryUseMap["total"] = m.TotalAlloc

		con, err := net.Dial("tcp", targAddr)
		if err != nil {
			fmt.Println("Error dialing for perf issues:", err, "Target addr:", targAddr)
		}

		defer con.Close()

		enc := gob.NewEncoder(con)

		encErr := enc.Encode(&p)
		if encErr != nil {
			fmt.Println("Error encoding in perf sendoff:", encErr)
		}

		con.Close()

		time.Sleep(400 * time.Nanosecond)
	}
}

func PrintLogger() {
	var m runtime.MemStats
	exeName := os.Args[0]
	for {
		f, err := os.OpenFile(exeName+".csv", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			panic(err)
		}

		defer f.Close()

		runtime.ReadMemStats(&m)

		if _, err = f.WriteString(fmt.Sprintf("%d,%d\n", time.Now().UnixNano(), m.TotalAlloc)); err != nil {
			panic(err)
		}

		time.Sleep(1 * time.Second)
	}
}
