package main

import (
	"fmt"
	"path/filepath"
	"strings"

	"gitlab.com/okotek/okoframe"
)

func ManageDir() {
	percChan := make(chan float64)
	tLen := 0
	//show status bar
	go func(pChan chan float64) {
		fmt.Println("Starting user directory read.")
		for {

			inp := <-percChan

			txt := ""

			calc := (inp / float64(tLen))
			amnt := ((calc * 100) / 3)

			for i := 0; i < int(amnt); i++ {
				txt = txt + "="
			}

			fmt.Printf("\r%s", txt)

		}
		fmt.Println("")

	}(percChan)

	var targ string
	if strings.HasSuffix("/", UserDir) {
		targ = UserDir + "*.okot"
	} else {
		targ = UserDir + "/*.okot"
	}

	glob, _ := filepath.Glob(targ)
	tLen = len(glob)

	for cnt, file := range glob {
		percChan <- float64(cnt)
		fr := okoframe.UnWrite(file)
		PointerList = append(PointerList, fr.ToStoragePointer())
	}

	defer func(){
		STOPPER.Done()
	}()

	
}
