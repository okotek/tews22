package main

import (
	"gitlab.com/okotek/okoframe"
)

var UserDir string
var MemCache okoframe.CacheUserFleet //Memcache for different frames
var PointerList []okoframe.StoragePointer

func Init(uDir string) {
	MemCache = make(okoframe.CacheUserFleet)
	UserDir = uDir
}
