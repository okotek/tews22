package main

import(
	"sync"
)
//Keep program from an hero-ing
var STOPPER sync.WaitGroup

//TLS stuff
var HTTPS bool
var CERT string
var KEY string 

//Let's Go
func main() {


	Init("USERS")
	go RunSvr()
	go ManageDir()

	STOPPER.Wait()

}
