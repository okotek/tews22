module gitlab.com/okotech/okofront

go 1.19

require gitlab.com/okotek/okoframe v1.1.1

require (
	gitlab.com/ashinnv/oddstring v1.1.1 // indirect
	gocv.io/x/gocv v0.31.0 // indirect
)

replace gitlab.com/okotek/okoframe => ../okoframe

replace gitlab.com/ashinnv/oddstring => ../oddstring
