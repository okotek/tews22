package main

import(
	"net/http"
)

func RunSvr(){

	http.HandleFunc("/", rt)          //root
	http.HandleFunc("/js", jsPlain)   //Plain JS
	http.HandleFunc("/jq", jQuery)    //Self-serve jquery
	http.HandleFunc("/img", imgs)     //Serve images (to include thumbnails)


	if HTTPS == false{
		http.ListenAndServe(":80", nil)
	}else{
		http.ListenAndServeTLS(":80", CERT, KEY, nil)
	}



}