package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"gocv.io/x/gocv"

	"okotech.net/x/okoconf"
	"okotech.net/x/okoframe"
	"okotech.net/x/okolog"
	"okotech.net/x/okonet"
)

type bytebox struct {
	Bts    []byte
	Uname  string
	Tstamp int64
	Chng   float32
	ImgNm  string
}

var cfg okoconf.CfgCont

func main() {

	go okolog.PrintLogger()

	cfg.SetDefault()

	sendErr, sendLog := okolog.InitLogServices(cfg)

	sendErr("HOE NOES! GO BIG PROBLM. Fake problem to check okolog.")
	sendLog("YAYS WE DID THESE THEINGS. Fake message to check okolog.")

	stageOne := make(chan okoframe.Frame)
	stageTwo := make(chan bytebox)

	go okonet.Listen(stageOne, ":8083", cfg)
	go buildBytes(stageOne, stageTwo)
	go storeBytes(stageTwo)

	stopper := make(chan bool)
	_ = <-stopper
}

// Encode a frame as a byte slice, then send that down a chan with some metadata
func buildBytes(input chan okoframe.Frame, output chan bytebox) {

	for tmp := range input {
		go func(frm okoframe.Frame) {

			//Encode the frame as a byteslice using gob
			retBytes := new(bytes.Buffer)
			enc := gob.NewEncoder(retBytes)
			err := enc.Encode(frm)
			if err != nil {
				fmt.Println(fmt.Errorf("Error doing the encoding: %e\n", err))
			}

			//Send encoded data down pipe
			output <- bytebox{retBytes.Bytes(), frm.UserName, frm.CreationTime.UnixNano(), frm.MotionPer, frm.Name}

			return
		}(tmp)
	}

}

/*
** StoreBytes() takes the input bytes that came from
** buildBytes() and writes them to disk.
**
** TODO: Compress data before writing it to disk.
 */
func storeBytes(input chan bytebox) {
	for inp := range input {

		oFrame := okoframe.FrameFromOkot(inp.Bts)
		oMat, _, _, _, _, mpr, errSubl := oFrame.Sublimate()
		if errSubl != nil {
			fmt.Println("Sublimate error:", errSubl)
		}

		mkerr := os.Mkdir(fmt.Sprintf("../%s", oFrame.UserName), 0777)
		if mkerr != nil {
			fmt.Println("Error making dir:", mkerr.Error)
			//os.Exit(1)
		}

		//writeMat, _ := oFrame.ToImage()
		//gocv.IMWrite("test.jpg", oMat)

		go func(inp bytebox, oMat gocv.Mat, mpr float32) {

			err3 := os.Mkdir(fmt.Sprintf("../%s", inp.Uname), 0777)
			if err3 != nil {
				fmt.Println("ERROR making dir:", err3)
			}

			if len(inp.Uname) < 3 {
				fmt.Println("Short username")
				inp.Uname = "DEAD_USER"
			}

			mkrErr := os.Mkdir("../"+inp.Uname, 0777)
			if mkrErr != nil {
				fmt.Println(mkrErr)
				time.Sleep(5 * time.Second)

			}

			targSave := fmt.Sprintf("/%s/%d_%f_%s_%f", inp.Uname, inp.Tstamp, inp.Chng, inp.ImgNm, mpr)
			targSave = strings.ReplaceAll(targSave, ".", "__")
			targSave = ".." + targSave + ".okot"

			//Write the byte slice to a binary file
			err := ioutil.WriteFile(targSave, inp.Bts, 0777)
			if err != nil {
				fmt.Println("Error writing bytes:", err)
			} else {
				fmt.Printf("Writing to: %s\n", targSave)
			}

			return
		}(inp, oMat, mpr)
	}
}

/*
** FrameFromBytes() takes input bytes formatted as
** they are when written to disk and decodes them
** into a frame.
 */
/*
func FrameFromBytes(input []byte) okoframe.Frame {

	frm := okoframe.Frame{}

	deBuf := bytes.NewBuffer(input)
	dec := gob.NewDecoder(deBuf)

	if decErr := dec.Decode(&frm); decErr != nil {
		fmt.Println("decErr fail in storeBytes(). Error:", decErr)
	}

	return frm

}
*/
