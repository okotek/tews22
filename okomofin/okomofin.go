package main

import (
	//"fmt"

	"fmt"
	"time"

	"okotech.net/x/okoconf"
	"okotech.net/x/okoframe"
	"okotech.net/x/okolog"
	"okotech.net/x/okonet"
)

var CFG okoconf.CfgCont
var ERR_LOG func(string)
var LOG_LOG func(string)

func main() {

	go okolog.PrintLogger()

	ERR_LOG, LOG_LOG = okolog.InitLogServices(CFG)
	stageOneChan := make(chan okoframe.Frame)
	stageTwoChan := make(chan okoframe.Frame)

	go okonet.Listen(stageOneChan, ":8081", CFG)
	go searchForMotion(stageOneChan, stageTwoChan)
	go okonet.Sendoff(stageTwoChan, ":8082", CFG)

	for {
		time.Sleep(1 * time.Hour)
	}
}

func searchForMotion(input chan okoframe.Frame, output chan okoframe.Frame) {
	/*

		NOTE: FIX THIS
		Using the method is not working for whatever reason.


		compFrm := <-input //Prime the channel
		for tmp := range input {
			tFrm := tmp
			//go func(tFrm okoframe.Frame) {
			go ERR_LOG("Looping over images.")
			dmp := tFrm.MotPer(compFrm, ERR_LOG, LOG_LOG)
			fmt.Printf("\r%v", dmp)
			//if dmp {
			output <- tmp
			fmt.Println("Sent")
			//}
			//}(tmp)
		}
	*/

	compFrm := <-input //priming the comparison frame so that we have something to compare against
	for tmp := range input {

		fmt.Println("Comparing:", compFrm.Name, " to ", tmp.Name)

		difCount := 1
		go LOG_LOG("Starting loop")

		for cnt := range tmp.MatBytes {
			diff := int(compFrm.MatBytes[cnt]) - int(tmp.MatBytes[cnt])

			//fmt.Printf("\rDoing byte: %d", cnt)
			//fmt.Printf("\rDif is: %d - %d = %d", int(compFrm.MatBytes[cnt]), int(tmp.MatBytes[cnt]), diff)

			if diff != 0 {
				difCount = difCount + 1
			}
		}

		fmt.Printf("\n\nDiff count:%d\n\n", difCount)

		dperc := (difCount / len(tmp.MatBytes)) * 100
		tmp.MotionPer = float32(dperc)

		_ = tmp.GetMotionPercent(compFrm)

		output <- tmp
	}
}
