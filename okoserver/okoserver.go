package main

func main(){

	var memcacheSize uint64 = 500 //Maximum of 500 MB memcache size 

	gConv = okoconf.SetDefault()
	

	go manageDb(memcacheSize)
	go listenReq()
	go serveHtm()
	go serveImgs()

	for{
		time.Sleep(1000 * time.Hour)
	}
}

//What it says on the tin. Keeps an eye on the files in the folder and serves them up if needed.
func manageDb(memcacheSize uint64){
	memcache := make(map[string]Frame)


	//Main loop for mdb()
	for{

		tempFrames := []okoframe.Frame{} //For storing 

		//Scan directory and update pointers
		fNames, globErr := filepath.Glbo(gConv.ServerDir + "/*.okot"
		if globErr != nil{
			fmt.Println("Error globbing okot:", globErr)
		}

		for _, name := range fNames{
			tFrame := okoframe.UnWrite(name)
			tempFrames = append(tempFrames, tFrame)
		}

		for _,frm := range tempFrames{
			//if memcacheOver(memcache){ //Is the memcache over limit? 
				if memcache.IsOver(){
					for i := 0; i < (int(memcache.Len) / 20)+ 1; i++{ //+1 is to prevent purging small memcaches
						memcache.Rem(memcache[i], i)
					}
				}
								
			//}
		}

		//Check memcache ages and replace if appropriate 

		//

	}


}

