package main //Yes, we can apparently redeclare main

//Pointer allows 
type pointer struct{
	Age         time.Duration //Last accessed 
	Tags        []string      //Classifier tags (revision is converted to string and included)
	Uname       string        //Owner's username
	Unit        string        //Unit that pointed to image came from
	Cam         string        //Camera that took pointed to image
	InMem       bool          //Is it in the memcache?
	ImgName     string        //Also memcache key
	Location    string        //Directory for the okot
	
}

//Memcaceh is a structure that holds cached frames. The other fields are to allow quick seaking. 
type memCache struct{
	Data	 map[string]okoframe.Frame
	InclAge  map[string]time.Time
	TimesFor []string          //For arranging frames by time, new to old. String is map key
	TimesRev []string          //For arranging frames by time, old to new
	Len      uint64
}

func (mc memCache)IsOver(){
	l := len(mc)
	if l > okoconf.GlobSvrLen{
		return true
	}else{
		return false
	}
}


func (mc memCache)Rem(tag string){
	delete(mc tag)
}

/*

func (mc memCache)GetSize() uint64{
	a := unsafe.Sizeof(mc.Data)
	b := unsafe.Sizeof(mc.Times)
	c := unsafe.Sizeof(mc.TimesRev)
}

/*
