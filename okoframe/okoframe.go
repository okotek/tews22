package okoframe

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"image"
	"io/ioutil"
	"time"

	"gitlab.com/ashinnv/oddstring"
)

func GenBlank() Frame {

	tmpMap := make(map[int]image.Rectangle)

	var ret Frame = Frame{
		MatBytes:     []byte{byte('3'), byte('4')},
		Wid:          3,
		Hei:          5,
		Name:         oddstring.RandStringSingleFull(100), // oddstring.ByteStringSimple(100),
		CreationTime: time.Now(),
		UserName:     "",
		MotionMap:    tmpMap,
		Detections:   make(map[string][]image.Rectangle),
		UnitName:     "",
		CamName:      "",
	}
	return ret
}

/*
** FrameFromOkot() takes input bytes formatted as
** they are when written to disk and decodes them
** into a frame.
 */
func FrameFromOkot(input []byte) Frame {

	frm := Frame{}

	deBuf := bytes.NewBuffer(input)
	dec := gob.NewDecoder(deBuf)

	if decErr := dec.Decode(&frm); decErr != nil {
		fmt.Println("decErr fail in storeBytes(). Error:", decErr)
	}

	return frm

}

// UnWrite() reads a .okot fileand returns a frame from that
func UnWrite(input string) Frame {
	dat, datErr := ioutil.ReadFile(input)
	if datErr != nil {
		fmt.Println("Error with reading okot data file:", datErr)
	}

	frm := FrameFromOkot(dat)

	return frm

}

/*
func GetMotionMap(input Frame, // The frame that is going to get the diff
	maskFrame Frame, // The frame that the first frame is compared to
	divisions int, // How many slices of the image to compare
	mode int) map[int]image.Rectangle { // Mode not implemented

	if mode == 666 {
		fmt.Println("devil")
	}

	splitMap := gocv.NewMatFromBytes(input.Hei, input.Wid, input.Type, input.MatBytes)

}
*/
/*
	Break up orgFrm into a grid of sub images and provide scores over those areas
*/
/*
func (orgFrm Frame) GetMotionMap(compFrm Frame, subImg int) error {
	var retErr error
	var orgPixAverages []byte
	var comPixAverages []byte

	//Get a per-pixel value average; turn a color image into black/white, essentially.
	if orgFrm.ChanCount == 3 {

		for i := 0; i < len(orgFrm.MatBytes); i += orgFrm.ChanCount {
			orgPixAverages = append(orgPixAverages, (orgFrm.MatBytes[i]+orgFrm.MatBytes[i+1]+orgFrm.MatBytes[i+2])/3)
		}

	} else if orgFrm.ChanCount == 1 {
		for i := 0; i < len(orgFrm.MatBytes); i += 1 {
			orgPixAverages = orgFrm.MatBytes
		}
	} else if orgFrm.ChanCount == 4 {

		for i := 0; i < len(orgFrm.MatBytes); i += orgFrm.ChanCount { //Skip the alpha channel
			orgPixAverages = append(orgPixAverages, (orgFrm.MatBytes[i]+orgFrm.MatBytes[i+1]+orgFrm.MatBytes[i+2])/3)
		}

	}

	//Same with comparison image
	if orgFrm.ChanCount == 3 {

		for i := 0; i < len(orgFrm.MatBytes); i += orgFrm.ChanCount {
			comPixAverages = append(comPixAverages, (orgFrm.MatBytes[i]+orgFrm.MatBytes[i+1]+orgFrm.MatBytes[i+2])/3)
		}

	} else if orgFrm.ChanCount == 1 {
		for i := 0; i < len(orgFrm.MatBytes); i += 1 {
			comPixAverages = orgFrm.MatBytes
		}
	} else if orgFrm.ChanCount == 4 {

		for i := 0; i < len(orgFrm.MatBytes); i += orgFrm.ChanCount { //Skip the alpha channel
			comPixAverages = append(comPixAverages, (orgFrm.MatBytes[i]+orgFrm.MatBytes[i+1]+orgFrm.MatBytes[i+2])/3)
		}

	}

	startWid := 0
	startHei := 0

	curPx := []int{0, 0}

	for row := 0; row < subImg; row += 1 {
		for col := 0; col < subImg; col += 1 {

			curPx[0] = row
			curPx[1] = col

			if col == subImg && row == subImg {

			}
		}
	}
	return retErr
}
*/
