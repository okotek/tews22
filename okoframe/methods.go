package okoframe

import (
	"fmt"
	"image"
	"time"

	"gocv.io/x/gocv"
	"gitlab.com/ashinnv/oddstring"
)

/*
** ToImage takes a Frame and returns an image.Image.
** The pipeline requires taking in a Frame, getting a
** GoCV mat from that, then using the GoCV built in
** method "ToImage" to return the image.
**
** If there's an issue with Sublimate, the method
** returns a nil Frame and an error, and if the issue
** is from ToImage, then we get exactly "toImage()"s
** error.
 */
func (orgFrm Frame) ToImage() (image.Image, error) {

	tmpMat, _, _, _, _, _, _ := orgFrm.Sublimate()

	return tmpMat.ToImage()

}

/*
Return a float32 percent of the image that has different pixels vs compImg.
This is considered on a channel byte basis, so if red and green
match, but blue doesn't, that counts as different. Returns an error
and includes that in the frame being evaluated.

Don't divide pixels by chans. Just get percent of pixels. The number
will be the same.
*/
func (orgFrm *Frame) MotPer(compImg Frame, logFuncs ...func(string)) bool {
	fmt.Println("Starting MotPer")
	var err error = nil

	diffByteCount := 0

	if orgFrm.ChanCount != compImg.ChanCount {
		orgFrm.MotionPer = 0
		err = fmt.Errorf("Could not compare org to comp frames. Chan count can not be compared.")
	}

	if len(orgFrm.MatBytes) != len(compImg.MatBytes) {
		orgFrm.MotionPer = 0
		err = fmt.Errorf("Could not compare org to comp frames. Byte maps not same length.")
	}

	fmt.Printf("\n==============\nByte length: %d\n=================\n", len(orgFrm.MatBytes))

	for byteCount := range orgFrm.MatBytes {

		fmt.Printf("\rDoing %d byte.", byteCount)

		if orgFrm.MatBytes[byteCount] != compImg.MatBytes[byteCount] {
			diffByteCount += 1
			fmt.Println("\nDiffCount: %d", diffByteCount)
		} else {
			fmt.Println("SameSame")
		}

		if len(logFuncs) > 1 {
			logFuncs[0](fmt.Sprintf("OrgFrame.MatBytes[bytecount]: %d CompImg.MatBytes[bytecount]: %d", orgFrm.MatBytes[byteCount], compImg.MatBytes[byteCount]))
		}
	}

	fmt.Println("Done iterating over bytes. Starting diffPixCount")
	var diffPixCount float32 = float32(diffByteCount / orgFrm.ChanCount) //Trying to make sure this stays float32. Compiler seemed to want to make it int.
	diffPerc := ((diffPixCount / float32((len(orgFrm.MatBytes))/orgFrm.ChanCount)) * 100.0)

	orgFrm.MotionPer = diffPerc

	if err != nil {
		orgFrm.ErrList = append(orgFrm.ErrList, fmt.Errorf("Error in MotPer: %e", err))
	}

	fmt.Println("Ending MotPer")
	return true
}

func (orgFrm *Frame) GetMotionPercent(compImg Frame) bool {
	diffByteCount := 0

	for count, bt := range compImg.MatBytes {
		if orgFrm.MatBytes[count] != bt {
			diffByteCount += 1
		}
	}

	fract := float32(diffByteCount / len(orgFrm.MatBytes))
	orgFrm.MotionPer = float32(fract * 100)
	fmt.Println("\n\nMotionPer: ", orgFrm.MotionPer)

	return true
}

// Decompose the frame into a gocv mat, username, password hash, motion map, detection map, and maybe some other stuff if I can think of it.
func (inFrm Frame) Sublimate() (gocv.Mat, string, []byte, map[int]image.Rectangle, map[string][]image.Rectangle, float32, error) {

	retMap := make(map[string][]image.Rectangle)
	retMot := make(map[int]image.Rectangle)

	retMap = inFrm.Detections
	retMot = inFrm.MotionMap
	mper := inFrm.MotionPer

	retMat, err := gocv.NewMatFromBytes(inFrm.Hei, inFrm.Wid, inFrm.Type, inFrm.MatBytes)

	return retMat, inFrm.UserName, inFrm.UserHash, retMot, retMap, mper, err

}

/*
** ConsumeMat() is the method equivalent to MatIntoFrame().
** This method allows an extant frame to consume a gocv.Mat
** Any properties of the frame that were already set will
** be replaced by the new Mat's data.
**
** TODO:HANDLE ERRORS!
 */
func (retFrm *Frame) ConsumeMat(inMat gocv.Mat) error {

	retFrm.ChanCount = inMat.Channels()
	retFrm.Wid = inMat.Cols()
	retFrm.Hei = inMat.Rows()
	retFrm.Type = inMat.Type()
	retFrm.CreationTime = time.Now()
	retFrm.MatBytes = inMat.ToBytes()
	retFrm.Name = oddstring.RandStringSingleFull(100)
	retFrm.MotionPer = 0

	return nil //We're not handling errors right now.
}

func FrameFromMat(inMat gocv.Mat) Frame {
	var retFrm Frame

	retFrm.ChanCount = inMat.Channels()
	retFrm.CreationTime = time.Now()
	retFrm.Hei = inMat.Rows()
	retFrm.Wid = inMat.Cols()
	retFrm.MatBytes = inMat.ToBytes()
	retFrm.Name = oddstring.RandStringSingleFull(100)
	retFrm.Type = inMat.Type()
	retFrm.MotionPer = 0
	retFrm.Detections = make(map[string][]image.Rectangle)

	return retFrm
}

func MatIntoFrame(retFrm Frame, inMat gocv.Mat) (Frame, error) {
	retFrm.ChanCount = inMat.Channels()
	retFrm.Wid = inMat.Cols()
	retFrm.Hei = inMat.Rows()
	retFrm.Type = inMat.Type()
	retFrm.CreationTime = time.Now()
	retFrm.MatBytes = inMat.ToBytes()
	retFrm.Name = oddstring.RandStringSingleFull(100)
	retFrm.MotionPer = 0

	return retFrm, nil
}

// Create an image storage pointer
func (fr Frame) ToStoragePointer() StoragePointer {

	dets := []string{}
	for k, _ := range fr.Detections {
		dets = append(dets, k)
	}
	sp := StoragePointer{
		FLoc:     "",
		Dets:     dets,
		MotPerc:  float64(fr.MotionPer),
		Uname:    fr.UserName,
		CamName:  fr.CamName,
		UnitName: fr.UnitName,
	}

	return sp
}
