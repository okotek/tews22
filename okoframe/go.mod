module okotech.net/x/okoframe

go 1.18

require (
	gitlab.com/ashinnv/oddstring v1.1.1
	gocv.io/x/gocv v0.31.0
)

replace asdf.com/x/okoMofinFuncs => ../okoMofinFuncs/

replace gitlab.com/ashinnv/oddstring => ../oddstring
