//For defining types

package okoframe

import (
	"image"
	"time"

	"gocv.io/x/gocv"
)

var GLOBALSTRINGLEN int = 35

// For storing the image caches for the user
type CacheUserFleet map[string]Frame

func (c CacheUserFleet) AddFrame(fr Frame) {
	camUnitName := fr.CamName + fr.UnitName
	c[camUnitName] = fr
}

type Frame struct {
	MatBytes     []byte //Opencv image bytes.
	ChanCount    int    //Color channels. Opencv uses this heavily.
	Wid          int
	Hei          int
	CreationTime time.Time
	UserName     string
	UserHash     []byte //Bcrypt password hash
	Type         gocv.MatType

	Name     string //Randomly generated string for individual image
	UnitName string
	CamName  string

	MotionMap  map[int]image.Rectangle      //int is percent of pixels in area changed. Rect is where the pixel change is
	MotionPer  float32                      //Total image percent of pixels changed vs a mask image that is assumed to have no subject.
	Detections map[string][]image.Rectangle //Rectangle slice is list of locations. String is name of classifier

	ErrList []error //Store errors that have happened with this frame.
}

// StoragePointer stores pointers to files by metadata
type StoragePointer struct {
	FLoc     string   //file location
	Dets     []string //Detections
	MotPerc  float64  //Percent of this frame that has changed vs its comp frame
	Uname    string   //
	CamName  string
	UnitName string
}
