package okoconf

import (
	"os"
)

type CfgCont struct {
	LoggerAddr       string
	LoggerListenPort string
	ErrorLoggerAddr  string
	ErrListenPort    string
	ExeName          string

	ServerDir        string //Directory that the server saves its files in
	SvrMemcacheLen   uint64 //Number of frames as maximum in FE server memcache
}

func (c *CfgCont) SetDefault() {
	c.LoggerAddr = "localhost:9091"
	c.LoggerListenPort = ":9091"
	c.ErrorLoggerAddr = "localhost:9092"
	c.ErrListenPort = ":9092"
	c.ExeName = "" //Depreciated

	//fmt.Println(c)

	//time.Sleep(4 * time.Second)
}

func SetConfig() CfgCont {
	var ret CfgCont

	ret.ExeName = os.Args[0]
	ret.LoggerAddr = "localhost:9091"

	return ret
}
