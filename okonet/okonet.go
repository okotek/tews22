package okonet

import (
	"encoding/gob"
	"errors"
	"fmt"
	"net"
	"os"
	"regexp"

	"okotech.net/x/okoconf"
	"okotech.net/x/okoframe"
)

//var LogChan chan int

func Listen(output chan okoframe.Frame, listenPort string, cfg okoconf.CfgCont) (error, string) {
	//go okolog.PrintLogger()

	dumpfile, err := os.OpenFile("ListenDump.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		fmt.Println("Error with Listen:", err)
	}

	defer dumpfile.Close()

	var genErr *error
	var locErr error
	var ln net.Listener
	var decFrame okoframe.Frame

	if !verifyListen(&listenPort, genErr) {
		fmt.Println("Unrecoverable error in verifying listen port:", *genErr)
	}

	ln, locErr = net.Listen("tcp", listenPort)
	if locErr != nil {
		fmt.Println("Error setting up listen:", locErr)
	} else {
		fmt.Println("Listening")
	}

	for {
		conn, accErr := ln.Accept()
		defer conn.Close()
		if accErr != nil {
			fmt.Println("Issue with accErr in ln.Accept():", accErr)
		} else {
			fmt.Println("Got con.")
		}

		fmt.Println("Starting connection.")
		dec := gob.NewDecoder(conn)

		dec.Decode(&decFrame)
		output <- decFrame
		conn.Close()

		if locErr != nil {
			fmt.Println("ListenError:", locErr)
		}
	}

	return *genErr, "fin"

}

func Sendoff(input chan okoframe.Frame, sendoffAddr string, cfg okoconf.CfgCont) {

	//Using defer here takes too long and we get the "too many files open error"
	//defer func() {
	//conn.Close()
	//}()

	for snd := range input {

		fmt.Println("New one sending.")
		var genErr error
		var conn net.Conn

		fmt.Println("Starting sendoff loop.")
		conn, genErr = net.Dial("tcp", sendoffAddr)
		defer conn.Close()

		if genErr != nil {
			fmt.Println("Error in sendoff dial:", genErr, "Data dumped.")
			conn.Close()
			Sendoff(input, sendoffAddr, cfg)
			return
		} else {
			fmt.Println("Connected to ", sendoffAddr)
		}

		enc := gob.NewEncoder(conn)
		encErr := enc.Encode(&snd)
		if encErr != nil {
			fmt.Println("Issue getting encode to work in sendoff:", encErr, "Data dumped.")
			conn.Close()
			Sendoff(input, sendoffAddr, cfg)
			return
		}
		fmt.Println("Sent off")
	}
}

func verifyListen(input *string, genErr *error) bool {
	match, _ := regexp.MatchString(":[0-9][0-9][0-9][0-9]|:[0-9][0-9][0-9]", *input)
	if match {
		return true
	}

	match, _ = regexp.MatchString(":[0-9]|:[0-9][0-9]", *input)
	if match {
		*genErr = errors.New("listen port too short. Use at least a 3-digit port number.")
		return false
	}

	return true
}
